package com.cgi.dentistapp.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.cgi.dentistapp.dao.entity.DentistEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    public DentistVisitService() {
    }

    public void addVisit(String dentistName, Date visitTime) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, visitTime);
        dentistVisitDao.create(visit);
    }

    public List<DentistVisitEntity> listVisits() {
        return dentistVisitDao.getAllVisits();
    }

    public List<DentistVisitEntity> searchVisits(String dentistName) {
        return dentistVisitDao.getSearch(dentistName);
    }

    public DentistVisitEntity getOne(long id) {
        return (DentistVisitEntity) dentistVisitDao.getOne(id);
    }

    public void delete(long id) {
        dentistVisitDao.delete(id);
    }

    public boolean checkTimeAvailable(String dentistName, Date start) {
        return dentistVisitDao.checkAvail(dentistName, start);
    }

}
