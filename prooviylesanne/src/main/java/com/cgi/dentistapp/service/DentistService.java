package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dao.DentistsDao;
import com.cgi.dentistapp.dao.entity.DentistEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by alvar on 12.04.2017.
 */
@Service
@Transactional
public class DentistService {

    @Autowired
    private DentistsDao dentistsDao;

    public DentistService() {
    }

    public void addDentist(String dentistName) {
        DentistEntity dentist = new DentistEntity(dentistName);
        dentistsDao.create(dentist);
    }

    public List<DentistEntity> listDentists () {
        return dentistsDao.getAllDentists();
    }
}
