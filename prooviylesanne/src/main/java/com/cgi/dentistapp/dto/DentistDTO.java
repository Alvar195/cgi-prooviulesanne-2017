package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by alvar on 12.04.2017.
 */
public class DentistDTO {

    @Size(min = 1, max = 50)
    String dentistName;

    public DentistDTO() {
    }

    public DentistDTO(String dentistName) {
        this.dentistName = dentistName;
    }

    public String getDentistName() {
        return dentistName;
    }

    public void setDentistName(String dentistName) {
        this.dentistName = dentistName;
    }
}