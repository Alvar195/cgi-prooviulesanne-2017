package com.cgi.dentistapp.dao.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by alvar on 12.04.2017.
 */
@Entity
@Table(name = "dentist")
public class DentistEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "dentistName")
    private String dentistName;

    public DentistEntity() {
    }

    public DentistEntity(String dentistName) {
        this.setDentistName(dentistName);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDentistName(String dentistName) {
        this.dentistName = dentistName;
    }

    public String getDentistName() {
        return dentistName;
    }
}
