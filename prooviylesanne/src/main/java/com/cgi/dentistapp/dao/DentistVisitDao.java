package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.util.Date;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }

    //search and get list of DentistVisitEntity by dentistName
    public List<DentistVisitEntity> getSearch(String dentistName) {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE dentistName = '" + dentistName + "'").getResultList();
    }

    //get one DentistVisitEntity by ID
    public Object getOne(Long id) {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE id = '" + id + "'").getSingleResult();
    }

    //delete DentistVisitEntity by ID
    public void delete(Long id) {
        Object o = entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE id = '" + id + "'").getSingleResult();
        entityManager.remove(o);
    }

    //check time window availability
    public boolean checkAvail(String dentistName, Date visitTime) {


        List<DentistVisitEntity> visits = getSearch(dentistName);
        if (visits.size() == 0) return true;

        long visitTimeStart = visitTime.getTime();
        long visitTimeEnd = visitTime.getTime() + 30 * 60 * 1000;

        for (DentistVisitEntity visit : visits) {
            long start = visit.getVisitTime().getTime();
            long end = start + 30 * 60 * 1000; // 30 minutes

            /*
             * LONG if statement incoming that uses xor.. don't see that everyday, atleast I don't.
             * This statement checks that start->end and new start->end does not overlap with existing one.
             * IntelliJ does offer to simplify it but for me this form is a lot more readable
             * Fun fact: I have made this exact if statement in my project http://24hworkload.com/ before :)
             */
            //Before existing one
            //new end can be same as old new
            if (!
                    (((start < visitTimeStart && end <= visitTimeStart) && (start < visitTimeEnd && end < visitTimeEnd))

                            ^ //xor

                            //After existing one
                            //old end can be same as new start
                            ((start > visitTimeStart && end > visitTimeStart) && (start >= visitTimeEnd && end > visitTimeEnd))))
                return false;
        }
        return true;
    }
}