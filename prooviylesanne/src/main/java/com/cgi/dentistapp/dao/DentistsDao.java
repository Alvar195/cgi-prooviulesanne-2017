package com.cgi.dentistapp.dao;

import com.cgi.dentistapp.dao.entity.DentistEntity;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by alvar on 12.04.2017.
 */
@Repository
public class DentistsDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistEntity dentist) {
        entityManager.persist(dentist);
    }

    public List<DentistEntity> getAllDentists() {
        return entityManager.createQuery("SELECT e FROM DentistEntity e").getResultList();
    }
}
