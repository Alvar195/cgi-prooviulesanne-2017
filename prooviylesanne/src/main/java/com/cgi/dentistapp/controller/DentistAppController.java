package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    @Autowired
    private DentistService dentistService;

    private boolean dentistSeed = false;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
        registry.addViewController("/visits").setViewName("visits");
        registry.addViewController("/doctors").setViewName("doctors");
        registry.addViewController("/detail").setViewName("detail");
        registry.addViewController("/delete").setViewName("detail");
        registry.addViewController("/edit").setViewName("edit");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("static/**").addResourceLocations("/static/css/**");
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO, Model model) {
        //seedDentists method generates a list of dentists that can be chosen
        if (!dentistSeed) seedDentists();

        //send a list of dentists to view
        model.addAttribute("dentists", dentistService.listDentists());
        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("dentists", dentistService.listDentists());
            return "form";
        }
        //if time window available then register
        if (dentistVisitService.checkTimeAvailable(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime())) {
            dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime());
            return "redirect:/results";
        } else {
            return "form";
        }
    }

    @GetMapping("/visits")
    public String showVisitsForm(Model model) {
        if (!dentistSeed) seedDentists();

        model.addAttribute("dentists", dentistService.listDentists());
        return "visits";
    }

    @GetMapping("/doctors")
    public String showDoctorsList(Model model) {
        if (!dentistSeed) seedDentists();

        model.addAttribute("dentists", dentistService.listDentists());
        return "doctors";
    }

    @PostMapping("/search")
    public String postSearchForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) {

        model.addAttribute("dentists", dentistService.listDentists());
        model.addAttribute("visits", dentistVisitService.searchVisits(dentistVisitDTO.getDentistName()));
        return "visits";
    }

    @RequestMapping("/detail")
    public String showDetail(@RequestParam(value = "id") long id, Model model) {
        if (!dentistSeed) seedDentists();

        model.addAttribute("visit", dentistVisitService.getOne(id));
        return "detail";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam(value = "id") long id, Model model) {
        if (!dentistSeed) seedDentists();

        dentistVisitService.delete(id);
        model.addAttribute("dentists", dentistService.listDentists());
        return "visits";
    }

    @RequestMapping("/edit")
    public String edit(@RequestParam(value = "id") long id, DentistVisitDTO dentistVisitDTO, Model model) {
        if (!dentistSeed) seedDentists();

        model.addAttribute("visit", dentistVisitService.getOne(id));
        model.addAttribute("dentists", dentistService.listDentists());
        return "edit";
    }

    @PostMapping("/save")
    public String save(@RequestParam(value = "id") long id, @Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) {
        if (!dentistSeed) seedDentists();
        if (bindingResult.hasErrors()) {
            model.addAttribute("visit", dentistVisitService.getOne(id));
            model.addAttribute("dentists", dentistService.listDentists());
            return "edit";
        }
        //decided to delete old and create new DentistVisit entity rather to change existing one
        dentistVisitService.delete(id);
        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime());

        model.addAttribute("dentists", dentistService.listDentists());
        return "visits";
    }

    //generate a list of dentists
    private void seedDentists() {
        dentistSeed = true; //do only once
        System.out.println("ADDING DENTISTS");
        //These may not be dentists but technically they are "doctors"
        dentistService.addDentist("Dr Dre");
        dentistService.addDentist("Dr House");
        dentistService.addDentist("Dr Phil");
        dentistService.addDentist("Dr Silva");
        dentistService.addDentist("Dr Watson");
        dentistService.addDentist("Dr Evil");
    }
}
